<?php

declare(strict_types=1);

namespace App\Integration\Validator;

use App\Integration\Exception\ClientException;

class ResponseValidator implements ResponseValidatorInterface
{
    public function validate($rawResponse)
    {
        if (!is_array($rawResponse) || (empty($rawResponse['data']) && empty($rawResponse['error']))) {
            throw new ClientException('Invalid client response!');
        }
    }
}
