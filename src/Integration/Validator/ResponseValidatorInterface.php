<?php

namespace App\Integration\Validator;

interface ResponseValidatorInterface
{
    public function validate($rawResponse);
}
