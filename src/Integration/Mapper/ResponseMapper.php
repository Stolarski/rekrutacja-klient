<?php

declare(strict_types=1);

namespace App\Integration\Mapper;

use App\Integration\Model\Response;

class ResponseMapper implements ResponseMapperInterface
{
    public function mapResponse($rawResponse): Response
    {
        if ($this->isResponseSuccess($rawResponse)) {
            return (new Response())
                ->setSuccess(true)
                ->setData($rawResponse['data']);
        }

        return (new Response())
            ->setSuccess(false)
            ->setError($rawResponse['error']);
    }

    private function isResponseSuccess($rawResponse): bool
    {
        return ($rawResponse['success'] === true)
            && $rawResponse['data'] !== null
            && empty($rawResponse['error']);
    }
}
