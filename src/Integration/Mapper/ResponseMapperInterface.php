<?php

namespace App\Integration\Mapper;

use App\Integration\Model\Response;

interface ResponseMapperInterface
{
    public function mapResponse($rawResponse): Response;
}
