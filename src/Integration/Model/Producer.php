<?php

declare(strict_types=1);

namespace App\Integration\Model;

class Producer
{
    private $id;

    private $name;

    private $siteUrl;

    private $logoFileName;

    private $ordering;

    private $sourceId;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    public function getSiteUrl(): string
    {
        return $this->siteUrl;
    }

    public function setSiteUrl(string $siteUrl)
    {
        $this->siteUrl = $siteUrl;

        return $this;
    }

    public function getLogoFileName(): string
    {
        return $this->logoFileName;
    }

    public function setLogoFileName(string $logoFileName)
    {
        $this->logoFileName = $logoFileName;

        return $this;
    }

    public function getOrdering(): int
    {
        return $this->ordering;
    }


    public function setOrdering(int $ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    public function getSourceId(): string
    {
        return $this->sourceId;
    }

    public function setSourceId(string $sourceId)
    {
        $this->sourceId = $sourceId;

        return $this;
    }
}
