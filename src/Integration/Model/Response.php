<?php

declare(strict_types=1);

namespace App\Integration\Model;

class Response
{
    private $success;

    private $data;

    private $error;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success)
    {
        $this->success = $success;

        return $this;
    }

    public function getError():?array
    {
        return $this->error;
    }

    public function setError(array $error)
    {
        $this->error = $error;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }
}
