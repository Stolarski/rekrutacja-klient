<?php

declare(strict_types=1);

namespace App\Integration\Model;

class Request
{
    const REQUEST_TYPE_GET = 'GET';
    const REQUEST_TYPE_POST = 'POST';

    private $url;

    private $type;

    private $bodyParameters;

    private $authorizationHeader;

    public function getAuthorizationHeader(): string
    {
        return $this->authorizationHeader;
    }

    public function setAuthorizationHeader(string $authorizationHeader)
    {
        $this->authorizationHeader = $authorizationHeader;
    }

    public function getBody(): ?array
    {
        return $this->bodyParameters;
    }

    public function setBody(array $bodyParameters)
    {
        $this->bodyParameters = $bodyParameters;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url)
    {
        $this->url = $url;

        return $this;
    }
}
