<?php

declare(strict_types=1);

namespace App\Integration;

use App\AuthorizationInterface;
use App\Integration\Mapper\ResponseMapper;
use App\Integration\Model\Response;
use App\Integration\Validator\ResponseValidator;
use App\RestClient\RestClient;

abstract class MethodAbstract
{
    protected $requestBuilder;

    protected $authorization;

    protected $restClient;

    protected function __construct(
        AuthorizationInterface $authorization,
        RequestBuilderInterface $requestBuilder)
    {
        $this->requestBuilder = $requestBuilder;
        $this->authorization = $authorization;

        /**
         * According to documentation - Response structure seems to be same for every method
         */
        $this->restClient = new RestClient(new ResponseValidator(), new ResponseMapper());
    }

    /**
     * Type of $data parameter depends on executed method
     */
    public function call($data = null): Response
    {
        $request = $this->requestBuilder->buildRequest($data);
        $request->setAuthorizationHeader($this->authorization->getAuthorizationHeader());

        return $this->restClient->request($request);
    }
}
