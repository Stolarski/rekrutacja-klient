<?php

declare(strict_types=1);

namespace App\Integration\Module\Producers\Method\CreateOne;

use App\Integration\Enum\UrlEnum;
use App\Integration\Model\Producer;
use App\Integration\Model\Request;
use App\Integration\RequestBuilderInterface;

class ProducersCreateOneRequestBuilder implements RequestBuilderInterface
{
    const URL = '/shop_api/v1/producers';

    /**
     * @param Producer $data
     * @return Request
     */
    public function buildRequest($data = null): Request
    {
        return (new Request())
            ->setUrl(UrlEnum::I_SKLEP_BASE_URL . self::URL)
            ->setType(Request::REQUEST_TYPE_POST)
            ->setBody($this->prepareBody($data));
    }

    private function prepareBody(Producer $producer): array
    {
        return [
            'producer' => [
                'id' => $producer->getId(),
                'name' => $producer->getName(),
                'site_url' => $producer->getSiteUrl(),
                'logo_filename' => $producer->getLogoFileName(),
                'ordering' => $producer->getOrdering(),
                'source_id' => $producer->getSourceId(),
            ]
        ];
    }
}
