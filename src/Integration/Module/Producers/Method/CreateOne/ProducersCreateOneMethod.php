<?php

declare(strict_types=1);

namespace App\Integration\Module\Producers\Method\CreateOne;

use App\AuthorizationInterface;
use App\Integration\MethodAbstract;

class ProducersCreateOneMethod extends MethodAbstract
{
    public function __construct(AuthorizationInterface $authorization)
    {
        parent::__construct($authorization, new ProducersCreateOneRequestBuilder());
    }
}
