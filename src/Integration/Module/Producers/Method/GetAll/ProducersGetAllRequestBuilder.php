<?php

declare(strict_types=1);

namespace App\Integration\Module\Producers\Method\GetAll;

use App\Integration\Enum\UrlEnum;
use App\Integration\Model\Request;
use App\Integration\RequestBuilderInterface;

class ProducersGetAllRequestBuilder implements RequestBuilderInterface
{
    const URL = '/shop_api/v1/producers';

    /**
     * @param null $data
     * @return Request
     */
    public function buildRequest($data = null): Request
    {
        return (new Request())
            ->setUrl(UrlEnum::I_SKLEP_BASE_URL . self::URL)
            ->setType(Request::REQUEST_TYPE_GET);
    }
}
