<?php

namespace App\Integration;

use App\Integration\Model\Request;

interface RequestBuilderInterface
{
    public function buildRequest($data = null): Request;
}
