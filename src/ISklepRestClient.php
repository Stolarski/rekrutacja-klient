<?php

declare(strict_types=1);

namespace App;

use App\Integration\Model\Producer;
use App\Integration\Model\Response;
use App\Integration\Module\Producers\Method\CreateOne\ProducersCreateOneMethod;
use App\Integration\Module\Producers\Method\GetAll\ProducersGetAllMethod;

class ISklepRestClient
{
    private $authorization;

    public function __construct(AuthorizationInterface $authorization)
    {
        $this->authorization = $authorization;
    }

    public function ProducersGetAll(): Response
    {
        return (new ProducersGetAllMethod($this->authorization))->call();
    }

    public function ProducersCreateOne(Producer $producer): Response
    {
        return (new ProducersCreateOneMethod($this->authorization))->call($producer);
    }
}
