<?php

declare(strict_types=1);

namespace App\RestClient;

use App\Integration\Mapper\ResponseMapperInterface;
use App\Integration\Model\Request;
use App\Integration\Model\Response;
use App\Integration\Validator\ResponseValidatorInterface;

class RestClient
{
    private $responseMapper;

    private $responseValidator;

    public function __construct(
        ResponseValidatorInterface $responseValidator,
        ResponseMapperInterface $responseMapper)
    {
        $this->responseMapper = $responseMapper;
        $this->responseValidator = $responseValidator;
    }

    public function request(Request $request): Response
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $request->getUrl());
        curl_setopt($curl, CURLOPT_USERPWD, $request->getAuthorizationHeader());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);

        if ($request->getType() == Request::REQUEST_TYPE_POST) {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request->getBody()));
        }

        $rawResponse = json_decode(curl_exec($curl), true);
        curl_close($curl);
        $this->responseValidator->validate($rawResponse);

        return $this->responseMapper->mapResponse($rawResponse);
    }
}
