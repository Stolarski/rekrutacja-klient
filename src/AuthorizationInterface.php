<?php

namespace App;

interface AuthorizationInterface
{
    public function getAuthorizationHeader(): string;
}
