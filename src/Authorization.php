<?php

declare(strict_types=1);

namespace App;

class Authorization implements AuthorizationInterface
{
    private $username;

    private $password;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function getAuthorizationHeader(): string
    {
        return $this->username . ':' . $this->password;
    }
}
