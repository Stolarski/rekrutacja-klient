**I-Sklep Client**

How To Use?
-
Every integrated method is available from **ISklepRestClient** class which is located in *src/*

It has to be constructed with **Authorization** which holds auth data.

Input data of **ISklepRestClient** functions vary from function to function.

If **ISklepRestClient** requires object on input, these objects are located in *src/Model*

Responses are returned as **Response** object

Adding new method integration
-
- create method dir in *src/Integration/Module* namespace (method namespace should match I-Sklep documentation structure)
- create **RequestBuilder** specified for new integration method
- create **Method** that extends **MethodAbstract** and injects new method request builder
- add function to **ISklepRestClient** accordingly to other functions

Tests
-
**phpspec** to run tests type in the console:
````
bin/phpspec run
````
