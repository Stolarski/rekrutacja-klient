<?php

namespace spec\App\Integration\Validator;

use App\Integration\Exception\ClientException;
use App\Integration\Validator\ResponseValidator;
use App\Integration\Validator\ResponseValidatorInterface;
use PhpSpec\ObjectBehavior;

class ResponseValidatorSpec extends ObjectBehavior
{


    function it_is_initializable()
    {
        $this->shouldHaveType(ResponseValidator::class);
    }

    function it_implements_interface()
    {
        $this->shouldHaveType(ResponseValidatorInterface::class);
    }

    function it_throws_exception_if_client_response_is_invalid()
    {
        $this->shouldThrow(ClientException::class)->duringValidate('invalid data');
        $this->shouldThrow(ClientException::class)->duringValidate(null);
        $this->shouldThrow(ClientException::class)->duringValidate(123);
        $this->shouldThrow(ClientException::class)->duringValidate(['invalidKey' => 'invalidValue']);
        $this->shouldThrow(ClientException::class)->duringValidate(['success' => true]);
    }

    function it_does_not_throw_exception_if_raw_response_is_valid()
    {
        $this->validate(['success' => true, 'data' => ['test1' => 123]]);
        $this->validate(['success' => false, 'error' => ['test1' => 123]]);
    }
}
