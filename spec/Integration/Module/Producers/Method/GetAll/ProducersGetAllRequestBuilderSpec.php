<?php

namespace spec\App\Integration\Module\Producers\Method\GetAll;

use App\Integration\Model\Request;
use App\Integration\Module\Producers\Method\GetAll\ProducersGetAllRequestBuilder;
use PhpSpec\ObjectBehavior;

class ProducersGetAllRequestBuilderSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ProducersGetAllRequestBuilder::class);
    }

    function it_creates_valid_request_object_with_type_get()
    {
        $returnedObject = $this->buildRequest();

        /** @var Request $returnedObject */
        $returnedObject->shouldHaveType(Request::class);
        $returnedObject->getUrl()->shouldBeString();
        $returnedObject->getType()->shouldBeEqualTo(Request::REQUEST_TYPE_GET);
        $returnedObject->getBody()->shouldBe(null);
    }
}
