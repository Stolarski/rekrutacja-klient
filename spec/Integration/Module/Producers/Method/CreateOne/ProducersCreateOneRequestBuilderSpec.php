<?php

namespace spec\App\Integration\Module\Producers\Method\CreateOne;

use App\Integration\Model\Producer;
use App\Integration\Model\Request;
use App\Integration\Module\Producers\Method\CreateOne\ProducersCreateOneRequestBuilder;
use PhpSpec\ObjectBehavior;
use TypeError;

class ProducersCreateOneRequestBuilderSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ProducersCreateOneRequestBuilder::class);
    }

    function it_throws_error_if_wrong_object_is_provided_to_method()
    {
        $this->shouldThrow(TypeError::class)->duringBuildRequest('wrong param');
        $this->shouldThrow(TypeError::class)->duringBuildRequest();
    }

    function it_creates_request_object_with_type_post_if_valid_producer_is_provided(Producer $producer)
    {
        $producer->getId()->willReturn(123);
        $producer->getSourceId()->willReturn('testSource');
        $producer->getOrdering()->willReturn(999);
        $producer->getLogoFileName()->willReturn('testLogoFileName');
        $producer->getSiteUrl()->willReturn('testSiteUrl');
        $producer->getName()->willReturn('testName');

        $returnedObject = $this->buildRequest($producer);

        /** @var Request $returnedObject */
        $returnedObject->shouldHaveType(Request::class);
        $returnedObject->getUrl()->shouldBeString();
        $returnedObject->getType()->shouldBeEqualTo(Request::REQUEST_TYPE_POST);
        $returnedObject->getBody()->shouldBeArray();
        $returnedObject->getBody()->shouldBeArray();
        $returnedObject->getBody()->shouldhaveKey('producer');
    }
}
