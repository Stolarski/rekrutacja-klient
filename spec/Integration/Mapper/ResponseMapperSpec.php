<?php

namespace spec\App\Integration\Mapper;

use App\Integration\Mapper\ResponseMapper;
use App\Integration\Mapper\ResponseMapperInterface;
use App\Integration\Model\Response;
use PhpSpec\ObjectBehavior;

class ResponseMapperSpec extends ObjectBehavior
{
    const SUCCESS_RAW_RESPONSE = ['success' => true, 'data' => ['test1' => 123]];
    const NOT_SUCCESS_RAW_RESPONSE = ['success' => false, 'error' => ['test1' => 123]];

    function it_is_initializable()
    {
        $this->shouldHaveType(ResponseMapper::class);
    }

    function it_implements_interface()
    {
        $this->shouldHaveType(ResponseMapperInterface::class);
    }

    function it_returns_response_as_object()
    {
        $this->mapResponse(self::SUCCESS_RAW_RESPONSE)->shouldHaveType(Response::class);
        $this->mapResponse(self::NOT_SUCCESS_RAW_RESPONSE)->shouldHaveType(Response::class);
    }

    function it_returns_response_with_filled_data_parameter_if_response_is_success()
    {
        /** @var Response $mappedResponse */
        $mappedResponse = $this->mapResponse(self::SUCCESS_RAW_RESPONSE);
        $mappedResponse->isSuccess()->shouldBe(true);
        $mappedResponse->getError()->shouldBe(null);
        $mappedResponse->getData()->shouldBeArray();
    }

    function it_returns_response_with_filled_error_parameter_if_response_is_not_success()
    {
        /** @var Response $mappedResponse */
        $mappedResponse = $this->mapResponse(self::NOT_SUCCESS_RAW_RESPONSE);
        $mappedResponse->isSuccess()->shouldBe(false);
        $mappedResponse->getError()->shouldBeArray();
        $mappedResponse->getData()->shouldBe(null);
    }
}
