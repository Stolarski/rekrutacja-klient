<?php

namespace spec\App;

use App\Authorization;
use App\ISklepRestClient;
use PhpSpec\ObjectBehavior;

class ISklepRestClientSpec extends ObjectBehavior
{
    function let(Authorization $authorization)
    {
        $this->beConstructedWith($authorization);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ISklepRestClient::class);
    }
}
